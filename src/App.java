import interfaces.Movable;
import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        Movable point1 = new MovablePoint(0, 0);
        Movable point2 = new MovablePoint(10, 10);

        System.out.println(point1.toString());
        System.out.println(point2.toString());

        point1.moveUp();
        point1.moveUp();

        point2.moveLeft();

        System.out.println(point1.toString());
        System.out.println(point2.toString());    
    }
}
